const express = require('express');

const {login} = require('../funciones/autenticacion.js');
const {validarUsuarioRegistrado} = require('../middlewares/middlewares_autenticacion.js')

function routerLogin()
	{
		const router = express.Router();		
		router.post('/login',validarUsuarioRegistrado,login);
		return router;
	}

module.exports={routerLogin};