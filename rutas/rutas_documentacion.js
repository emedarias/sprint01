const  yaml  =  require ('js-yaml');
const fs = require('fs');
const swaggerUI = require('swagger-ui-express');

function cargarDocumentacion(app)
    {
        try 
            { 
                const docu = yaml.load (fs.readFileSync('./datos/documentacion.yml','utf8')); 
                app.use('/api/v1/api-docs', swaggerUI.serve, swaggerUI.setup(docu));
            } 
        catch (e)
            { 
                console.log (e) ; 
            }
    }

module.exports = {cargarDocumentacion}