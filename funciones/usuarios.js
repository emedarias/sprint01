const {usuarios} = require("../datos/datos.js");

//funcion para ver usuarios. Debe sacarse en la produccion.
function getUsuarios(req, res)
	{
		if(usuarios)
			{
				return res.status(200).send(usuarios);
			}
		return res.status(404).send("No hay usuarios registrados.");
	}

//funcion para crear usuarios
function postUsuario(req, res)
	{
		let id;
		if(usuarios.length>0)
			{
				id=usuarios[usuarios.length-1].id+1;		
			}
		else
			{
				id=0;	
			}
		const nombre = req.body.nombre;
		const direccion = req.body.direccion;
		const mail = req.body.mail;
		const telefono = req.body.telefono;
		const password = req.body.pass1;
		const admin = req.body.admin;
		
		const nuevoUsuario={id, nombre, direccion, mail, telefono, password, admin};
		
		usuarios.push(nuevoUsuario);
		return res.status(200).send("El usuario fue creado con exito.");		
	}

module.exports={getUsuarios, postUsuario};