const {productos} = require("../datos/datos.js");

//funcion para ver productos. Debe sacarse en la produccion.
function getProductos(req, res)
	{
		if(productos)
			{
				return res.status(200).send(productos);
			}
		return res.status(404).send("No hay productos registrados.");
	}

//funcion para crear productos
function postProducto(req, res)
	{
		let id;
		if(productos.lenght)
			{
				id=productos[productos.length-1].id+1;
			}
		else
			{
				id=0;
			}
		
		const nombre = req.body.nombre;
		const precio = req.body.precio;
		
		const nuevoProducto={id, nombre, precio};
		
		productos.push(nuevoProducto);
		return res.status(200).send("El producto fue creado con exito.");		
	}

//funcion para editar productos
function putProducto(req, res)
	{
		for(const producto in productos)
			{
				if(productos[producto].id === Number(req.body.id))
					{
						productos[producto].nombre=req.body.nombre;
						productos[producto].precio=Number(req.body.precio);
						return res.status(200).send("El producto fue editado con exito.");	
					}
			}	
	}

//funcion para eliminar productos
function deleteProducto(req, res)
	{
		let eliminar;

		for(const producto in productos)
			{
				if(productos[producto].id === Number(req.body.id))
					{
						 eliminar = Number(producto);
					}
			}

		productos.splice(eliminar,1);
		return res.status(201).send("El producto fue eliminado con exito.");			
	}

module.exports={getProductos, postProducto, putProducto, deleteProducto};