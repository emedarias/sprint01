const {pedidos, metodosPagos, estadoPedidos, productos, usuarios} = require("../datos/datos.js");


function getPedidos(req, res)
	{
		if(pedidos)
			{
				return res.status(200).send(pedidos);
			}
		return res.status(404).send("No hay pedidos registrados.");
	}


function getEstadoPedido(req, res)
	{
		let pedidoBuscado;
		for(const pedido of pedidos)
			{
				if(pedido.id === Number(req.body.id))
					{
						pedidoBuscado=pedido;
					}
			}

		const id = pedidoBuscado.id;
		const direccion = pedidoBuscado.direccion;
		const telefono = pedidoBuscado.telefono;
		const mail = pedidoBuscado.mail;
		const total = pedidoBuscado.totalPagar;
		const detalle = pedidoBuscado.productosPedido;

		let estadoPedido;
		for(const estado of estadoPedidos)
			{
				if(estado.id === Number(pedidoBuscado.estadoId))
					{
						estadoPedido=estado.estado;
					}
			}

		let pagoPedido;
		for(const pago of metodosPagos)
			{
				if(pago.id===Number(pedidoBuscado.pagoId))
					{
						pagoPedido=pago.metodo;
					}
			}

		const respuestaPedido = {id, estadoPedido, pagoPedido, direccion, telefono, mail, detalle, total};		
		return res.status(200).send(respuestaPedido	);
	}


function postPedidos(req, res)
	{
		let user;
		for(const usuario of usuarios)
			{
				if (usuario.mail === req.headers.mail)
					{
						user = usuario;
					}
			}

		const nuevoId= new Date;
		const id = nuevoId.getTime();
		const estadoId = 1;
		const pagoId=req.body.pagoId;
		let direccion;
		if(req.body.direccion!=="" && req.body.direccion!==null && req.body.direccion!==undefined)
			{
				direccion=req.body.direccion	
			}
		else
			{
				direccion=user.direccion;
			}
		const telefono= user.telefono;
		const mail = user.mail;
		const productosRecibidos = req.body.productos;
		const productosPedido = [];
		let totalPagar = 0;
		for (const productoRecibido of productosRecibidos)
			{	
				for(const producto of productos)
					{
						if(productoRecibido.id===producto.id)
							{
								const nombre = producto.nombre;
								const precio = producto.precio;
								const cantidad = productoRecibido.cantidad;
								const nuevoProducto={nombre,precio,cantidad};
								productosPedido.push(nuevoProducto);
								totalPagar+= producto.precio * productoRecibido.cantidad;
							}
					}
			}
		const nuevoPedido = {id, estadoId, pagoId, direccion, telefono, mail, productosPedido, totalPagar};
		pedidos.push(nuevoPedido);
		return res.status(200).send("El pedido fue creado con exito.");
	}


function putPedidoUsuario(req, res)
	{
		let pedidoBuscado;
		for(const pedido of pedidos)
			{
				if(pedido.id === Number(req.body.id))
					{
						pedidoBuscado=pedido;
					}
			}

		let user;
		for(const usuario of usuarios)
			{
				if (usuario.mail === req.headers.mail)
					{
						user = usuario;
					}
			}

		if(Number(req.body.estadoId)<3 && Number(req.body.estadoId)>0)
			{
				pedidoBuscado.estadoId = req.body.estadoId;
			}

		pedidoBuscado.pagoId = req.body.pagoId;
		
		let direccion;
		if(req.body.direccion!=="" && req.body.direccion!==null && req.body.direccion!==undefined)
			{
				direccion=req.body.direccion	
			}
		else
			{
				direccion=user.direccion;
			}

		pedidoBuscado.direccion = direccion;
		pedidoBuscado.mail = user.mail;
		pedidoBuscado.telefono = user.telefono;

		const productosRecibidos = req.body.productos;
		const productosPedido = [];
		let totalPagar = 0;
		for (const productoRecibido of productosRecibidos)
			{	
				for(const producto of productos)
					{
						if(productoRecibido.id===producto.id)
							{
								const nombre = producto.nombre;
								const precio = producto.precio;
								const cantidad = productoRecibido.cantidad;
								const nuevoProducto={nombre,precio,cantidad};
								productosPedido.push(nuevoProducto);
								totalPagar+= producto.precio * productoRecibido.cantidad;
							}
					}
			}
		pedidoBuscado.productosPedido=productosPedido;
		pedidoBuscado.totalPagar=totalPagar;

		return res.status(201).send("El pedido fue actualizado con exito.");
	}

function putPedidoAdmin(req, res)
	{
		let pedidoBuscado;
		for(const pedido of pedidos)
			{
				if(pedido.id === Number(req.body.id))
					{
						pedidoBuscado=pedido;
					}
			}

		if(pedidoBuscado.estadoId<7 && pedidoBuscado.estadoId>0)
			{
				pedidoBuscado.estadoId = req.body.estadoId;
				return res.status(201).send("El estado del pedido fue actualizado con exito.");		
			}
		else
			{
				return res.status(400).send("El estado del pedido no se pudo actualizar.");		
			}
	}

module.exports={getPedidos,getEstadoPedido,postPedidos,putPedidoUsuario,putPedidoAdmin};