const {metodosPagos} = require("../datos/datos.js");

//Funcion para validar si el metodo de pago esta registrado por nombre.
function validarPagoRegistrado(req, res, next)
	{
		for(const metodo of metodosPagos)
			{
				if(metodo.metodo === req.body.metodo)
					{
						return res.status(406).send("El metodo de pago ya esta registrado.");
					}
			}
		return next();
	};

//Funcion para validar si el metodo de pago existe por id.
function validarPagoExistente(req, res, next)
	{
		for(const metodo of metodosPagos)
			{
				if(metodo.id === Number(req.body.id))
					{
						return next();
					}
			}
		return res.status(404).send("El metodo de pago que busca no esta registrado.");
	};


module.exports={validarPagoRegistrado, validarPagoExistente};