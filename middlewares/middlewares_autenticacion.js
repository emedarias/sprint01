const {usuarios, autenticados} = require("../datos/datos.js");

//Funcion para validar si el mail esta registrado.
function validarUsuarioRegistrado(req, res, next)
	{
		for(const usuario of usuarios)
			{
				if(usuario.mail === req.body.mail )
					{
						return next();				
					}
			}
		return res.status(404).send("El mail no esta registrado.");
	};

function validarUsuarioAutenticado(req, res, next)
	{
		for(const autenticado of autenticados)
			{
				if(usuarios[autenticado].mail === req.headers.mail && usuarios[autenticado].password === req.headers.password )
					{
						return next();				
					}
			}
		return res.status(401).send("El usuario no esta autenticado.");
	};


function validarUsuarioAdmin(req, res, next)
	{
		for(const autenticado of autenticados)
			{
				if(usuarios[autenticado].mail === req.headers.mail && usuarios[autenticado].password === req.headers.password )
					{
						
						if(usuarios[autenticado].admin==true)
							{
								return next();		
							}
					}
			}
		return res.status(403).send("El usuario no tiene permisos suficientes.");
	};

module.exports={validarUsuarioRegistrado, validarUsuarioAutenticado, validarUsuarioAdmin};