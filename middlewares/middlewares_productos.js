const {productos} = require("../datos/datos.js");

//Funcion para validar si el producto esta registrado por nombre.
function validarProductoRegistrado(req, res, next)
	{
		for(const producto of productos)
			{
				if(producto.nombre === req.body.nombre)
					{
						return res.status(406).send("El producto ya esta registrado.");
					}
			}
		return next();
	};

//Funcion para validar si el producto existe por id.
function validarProductoExistente(req, res, next)
	{
		for(const producto of productos)
			{
				if(producto.id === Number(req.body.id))
					{
						return next();
					}
			}
		return res.status(404).send("El producto que busca no esta registrado.");
	};


module.exports={validarProductoRegistrado, validarProductoExistente};