const {usuarios} = require("../datos/datos.js");

//Funcion para validar si el mail esta registrado.
function validarMailExistente(req, res, next)
	{
		if(usuarios.length===0)
			{
				return next();				
			}
		else
			{
				for(const usuario of usuarios)
					{
						if(usuario.mail === req.body.mail)
							{
								return res.status(400).send("El mail ya esta registrado.");
							}
					}
					
				return next();		
			}
		
	};

//Funcion para validar password.
function validarPass(req, res, next)
	{
		if(req.body.pass1 !== req.body.pass2)
			{
				return res.status(406).send("Los passwords no son iguales.");
			}
		return next();
	}

module.exports={validarMailExistente, validarPass};