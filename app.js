const express = require('express');		
const app = express();

const {routerUsuarios} = require('./rutas/rutas_usuarios.js');
const {routerProductos} = require('./rutas/rutas_productos.js');
const {routerLogin} = require('./rutas/rutas_autenticacion.js');
const {routerPagos} = require('./rutas/rutas_pagos.js');
const {routerPedidos} = require('./rutas/rutas_pedidos.js');
const {cargarDocumentacion} = require('./rutas/rutas_documentacion.js');

const version ='/api/v1';

app.use(express.json());
cargarDocumentacion(app);

app.use(version, routerUsuarios());
app.use(version, routerLogin());
app.use(version, routerProductos());
app.use(version, routerPagos());
app.use(version, routerPedidos());

app.listen('9000',() => {console.log("Delilah listo en 'localhost:9000/api/v1/'")});